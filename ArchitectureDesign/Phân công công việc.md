#Phân công công việc

**1. Fix Usecase tổng quan** 

* Quốc + Sơn

**2. Đặc tả lại UC của AFC**

* `UC001: Soát thẻ ra` + `UC002: Soát thẻ vào` → **YOS SAROEUN**
* `UC003: Soát vé vào` + `UC004: Soát vé ra` → **RATHANA**

**3. Activity diagrams** 

* `UC001: Soát thẻ ra` + `UC002: Soát thẻ vào` → **YOS SAROEUN**
* `UC003: Soát vé vào` + `UC004: Soát vé ra` → **RATHANA**

**3. Sequence diagrams** 

* **QUỐC**

**4. Class diagrams** 

* **SƠN**


#Tiến độ công việc
Do tính chất công việc và bận bịu của 2 bạn nước ngoài **Rathana** và **YOS SAROEUN**, team phải phân chia công việc như này

* Rathana: Hoàn thành đúng hẹn
* Sơn: Hoàn thành đúng hẹn
* Yos Saroeun: Hoàn thành đúng hẹn
* Quốc: Hoàn thành đúng hẹn
