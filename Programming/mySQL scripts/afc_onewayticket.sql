-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: afc
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `onewayticket`
--

DROP TABLE IF EXISTS `onewayticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `onewayticket` (
  `id` char(20) NOT NULL,
  `code` char(20) DEFAULT NULL,
  `customerId` int(11) DEFAULT NULL,
  `checkInStationId` int(11) DEFAULT NULL,
  `checkOutStationId` int(11) DEFAULT NULL,
  `defaultInStationId` int(11) NOT NULL,
  `defaultOutStationId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `customerId` (`customerId`),
  KEY `inStationId` (`checkInStationId`),
  KEY `outStationId` (`checkOutStationId`),
  KEY `fk_onewayticket_station1_idx` (`defaultInStationId`),
  KEY `fk_onewayticket_station2_idx` (`defaultOutStationId`),
  CONSTRAINT `fk_onewayticket_station1` FOREIGN KEY (`defaultInStationId`) REFERENCES `station` (`id`),
  CONSTRAINT `fk_onewayticket_station2` FOREIGN KEY (`defaultOutStationId`) REFERENCES `station` (`id`),
  CONSTRAINT `onewayticket_ibfk_1` FOREIGN KEY (`customerId`) REFERENCES `customer` (`id`),
  CONSTRAINT `onewayticket_ibfk_2` FOREIGN KEY (`checkInStationId`) REFERENCES `station` (`id`),
  CONSTRAINT `onewayticket_ibfk_3` FOREIGN KEY (`checkOutStationId`) REFERENCES `station` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `onewayticket`
--

LOCK TABLES `onewayticket` WRITE;
/*!40000 ALTER TABLE `onewayticket` DISABLE KEYS */;
INSERT INTO `onewayticket` VALUES ('OW201910300001','3dbe00a167653a1a',1,24,25,23,26),('OW20191103002','810247419084c82d',1,NULL,NULL,20,25);
/*!40000 ALTER TABLE `onewayticket` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-04 23:42:22
