/**
 * 
 */
package hust.tkxdpm.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import hust.tkxdpm.entities.Customer;
import hust.tkxdpm.entities.Station;
import hust.tkxdpm.service.DBConnection;

/**
 * @author SpQuyt
 * @date 4 Dec 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public class OtherQuery {
	private static Statement statement = new DBConnection().getStatement();

	/**
	 * Insert new history path to database.
	 * 
	 * @param inStation  in station of customer
	 * @param outStation out station of customer
	 * @param customer   a Customer entity
	 * @throws ClassNotFoundException if no class would be found
	 * @throws SQLException           if there was an error with SQL query execution
	 */
	public void insertNewHistoryPath(Station inStation, Station outStation, Customer customer)
			throws ClassNotFoundException, SQLException {
		String query = String.format(
				"insert into historypath(checkInStationId, checkOutStationId, customerId) values (%d, %d, %d)",
				inStation.getStationId(), outStation.getStationId(), customer.getCustomerId());
		statement.executeUpdate(query);
	}

	/**
	 * Query distance between 2 adjacent stations.
	 * 
	 * @param inStation  in station of customer
	 * @param outStation out station of customer
	 * @return value of distance
	 * @throws ClassNotFoundException if no class would be found
	 * @throws SQLException           if there was an error with SQL query execution
	 */
	public float queryDistance(Station inStation, Station outStation) throws ClassNotFoundException, SQLException {
		String query = "select * from distance where inStationId = " + inStation.getStationId() + " and outStationId = "
				+ outStation.getStationId();
		ResultSet resultSet = statement.executeQuery(query);

		if (resultSet.next()) {
			return resultSet.getFloat("value");
		} else {
			return 0;
		}
	}
}
