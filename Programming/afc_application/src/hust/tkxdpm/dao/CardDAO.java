package hust.tkxdpm.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import hust.tkxdpm.entities.Card;
import hust.tkxdpm.entities.Customer;
import hust.tkxdpm.entities.PrepaidCard;
import hust.tkxdpm.entities.Station;
import hust.tkxdpm.service.DBConnection;

/**
 * @author SpQuyt
 * @date Nov 4, 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public class CardDAO {
	private static Statement statement = new DBConnection().getStatement();
	private CustomerDAO customerDao = new CustomerDAO();
	private StationDAO stationDao = new StationDAO();
	
	/**
	 * Query card's info from database.
	 * @param cardcode which has 14 digits.
	 * @return a Card entity or null
	 * @throws ClassNotFoundException if no class would be found
	 * @throws SQLException if there was an error with SQL query execution
	 */
	public PrepaidCard queryPPCardInfo(String cardcode) throws ClassNotFoundException, SQLException {
		String query = "select * from card where code = '" + cardcode + "'";
		ResultSet resultSet = statement.executeQuery(query);

		if (resultSet.next()) {
			Customer customer = customerDao.queryCustomerInfo(resultSet.getInt("customerId"));
			Station station = stationDao.queryStationInfo(resultSet.getInt("checkInStationId"));
			String id = resultSet.getString("id");
			double balance = resultSet.getDouble("balance");
			return new PrepaidCard(id, cardcode, customer, balance, station);
		} else {
			return null;
		}
	}

	/**
	 * Update Card's inStation to CURRENTSTATION on database.
	 * @param cardcode which has 14 digits.
	 * @param currentStation the station which customer is choosing.
	 * @throws ClassNotFoundException if no class would be found
	 * @throws SQLException if there was an error with SQL query execution
	 */
	public void updateInStationPPCard(String cardcode, Station currentStation)
			throws ClassNotFoundException, SQLException {
		String update = "Update card set checkInStationId = " + currentStation.getStationId() + " where code = '" + cardcode
				+ "'";
		statement.executeUpdate(update);
	}

	/**
	 * Update Card's inStation to NULL on database + Update Card's balance.
	 * @param cardcode which has 14 digits.
	 * @param balanceOut balance after reducing.
	 * @throws ClassNotFoundException if no class would be found
	 * @throws SQLException if there was an error with SQL query execution
	 */
	public void updateOutStationPPCard(String cardcode, double balanceOut) throws ClassNotFoundException, SQLException {
		String updateNullInStation = "Update card set checkInStationId = NULL where code = '" + cardcode + "'";
		String updateBalance = "Update card set balance = " + balanceOut + " where code = '" + cardcode + "'";
		statement.executeUpdate(updateNullInStation);
		statement.executeUpdate(updateBalance);
	}
}
