package hust.tkxdpm.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import hust.tkxdpm.entities.Customer;
import hust.tkxdpm.service.DBConnection;

/**
 * @author SpQuyt
 * @date Nov 6, 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public class CustomerDAO {
	private static Statement statement = new DBConnection().getStatement();
	
	/**
	 * Query customer's info from database.
	 * @param id which is customer's id.
	 * @return a Customer entity or null
	 * @throws ClassNotFoundException if no class would be found
	 * @throws SQLException if there was an error with SQL query execution
	 */
	public Customer queryCustomerInfo(int id) throws ClassNotFoundException, SQLException {
		String query = "select * from customer where id=" + id;
		ResultSet resultSet = statement.executeQuery(query);

		if (resultSet.next()) {
			String name = resultSet.getString("name");
			String phone = resultSet.getString("phone");
			return new Customer(id, name, phone);
		} else {
			return null;
		}
	}
}
