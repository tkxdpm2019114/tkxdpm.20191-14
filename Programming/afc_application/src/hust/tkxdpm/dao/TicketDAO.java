package hust.tkxdpm.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import hust.tkxdpm.entities.Customer;
import hust.tkxdpm.entities.OneWayTicket;
import hust.tkxdpm.entities.Station;
import hust.tkxdpm.entities.TwentyFourTicket;
import hust.tkxdpm.service.DBConnection;

/**
 * @author SpQuyt
 * @date Nov 6, 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public class TicketDAO {
	private static Statement statement = new DBConnection().getStatement();
	private CustomerDAO customerDao = new CustomerDAO();
	private StationDAO stationDao = new StationDAO();
	
	/**
	 * Query One-Way ticket's info from database.
	 * @param code which has 14 digits.
	 * @return a OneWayTicket entity or null
	 * @throws ClassNotFoundException if no class would be found
	 * @throws SQLException if there was an error with SQL query execution
	 */
	public OneWayTicket queryOWTicketInfo(String code) throws ClassNotFoundException, SQLException {
		String query = "select * from onewayticket where code = '" + code + "'";
		ResultSet resultSet = statement.executeQuery(query);
		if (resultSet.next()) {
			Customer customer = customerDao.queryCustomerInfo(resultSet.getInt("customerId"));
			Station stationIn = stationDao.queryStationInfo(resultSet.getInt("checkInStationId"));
			Station stationOut = stationDao.queryStationInfo(resultSet.getInt("checkOutStationId"));
			Station stationdefaultIn = stationDao.queryStationInfo(resultSet.getInt("defaultInStationId"));
			Station stationdefaultOut = stationDao.queryStationInfo(resultSet.getInt("defaultOutStationId"));
			String id = resultSet.getString("id");
			String ticketcode = resultSet.getString("code");
			return new OneWayTicket(id, ticketcode, customer, stationIn, stationOut, stationdefaultIn, stationdefaultOut);
		} else {
			return null;
		}
	}

	/**
	 * Update InStation for One-Way ticket from database.
	 * @param code which has 14 digits.
	 * @param currentStation current station which customer is going
	 * @throws ClassNotFoundException if no class would be found
	 * @throws SQLException if there was an error with SQL query execution
	 */
	public void updateInStationOWTicket(String code, Station currentStation) throws ClassNotFoundException, SQLException {
		String update = "Update onewayticket set checkInStationId = " + currentStation.getStationId()
				+ " where code = '" + code + "'";
		statement.executeUpdate(update);

	}

	/**
	 * Update OutStation for One-Way ticket from database.
	 * @param code which has 14 digits.
	 * @param currentStation current station which customer is going
	 * @throws ClassNotFoundException if no class would be found
	 * @throws SQLException if there was an error with SQL query execution
	 */
	public void updateOutStationOWTicket(String code, Station currentStation) throws ClassNotFoundException, SQLException {
		String update = "Update onewayticket set checkOutStationId = " + currentStation.getStationId()
				+ " where code = '" + code + "'";
		statement.executeUpdate(update);
	}
	
	
	/**
	 * Query Twenty-Four-Hour ticket's info from database.
	 * @param code which has 14 digits.
	 * @return a TwentyFourTicket entity or null
	 * @throws ClassNotFoundException if no class would be found
	 * @throws SQLException if there was an error with SQL query execution
	 */
	public TwentyFourTicket queryTFTicketInfo(String code) throws ClassNotFoundException, SQLException {
		String query = "select * from 24hticket where code = '" + code + "'";
		ResultSet resultSet = statement.executeQuery(query);
		if (resultSet.next()) {
			Customer customer = customerDao.queryCustomerInfo(resultSet.getInt("customerId"));
			Station stationIn = stationDao.queryStationInfo(resultSet.getInt("checkInStationId"));
			Timestamp expiredTime = resultSet.getTimestamp("expiredTime");
			String id = resultSet.getString("id");
			String ticketcode = resultSet.getString("code");

			return new TwentyFourTicket(id, ticketcode, customer, stationIn, expiredTime);
		} else {
			return null;
		}
	}

	/**
	 * Update InStation for Twenty-Four ticket from database.
	 * @param code which has 14 digits.
	 * @param currentStation current station which customer is going
	 * @throws ClassNotFoundException if no class would be found
	 * @throws SQLException if there was an error with SQL query execution
	 */
	public void updateInStationTFTicket(String code, Station currentStation) throws ClassNotFoundException, SQLException {
		String update = "Update 24hticket set checkInStationId = " + currentStation.getStationId()
				+ " where code = '" + code + "'";
		statement.executeUpdate(update);
	}

	/**
	 * Update OutStation for Twenty-Four ticket from database.
	 * @param code which has 14 digits.
	 * @param currentStation current station which customer is going
	 * @throws ClassNotFoundException if no class would be found
	 * @throws SQLException if there was an error with SQL query execution
	 */
	public void updateOutStationTFTicket(String code) throws ClassNotFoundException, SQLException {
		String update = "Update 24hticket set checkInStationId = NULL where code = '" + code + "'";
		statement.executeUpdate(update);
	}
}
