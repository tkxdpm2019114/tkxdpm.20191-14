package hust.tkxdpm.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import hust.tkxdpm.entities.Station;
import hust.tkxdpm.service.DBConnection;

/**
 * @author SpQuyt
 * @date Nov 6, 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public class StationDAO {
	private Statement statement = new DBConnection().getStatement();
	
	/**
	 * Query all stations from database.
	 * @return an ArrayList of Station entities or null
	 * @throws ClassNotFoundException if no class would be found
	 * @throws SQLException if there was an error with SQL query execution
	 */
	public ArrayList<Station> queryAllStations() throws ClassNotFoundException, SQLException {
		String query = "select * from station";
		ResultSet resultSet = statement.executeQuery(query);
		ArrayList<Station> listStations = new ArrayList<Station>();

		while (resultSet.next()) {
			listStations.add(
					new Station(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getString("address")));
		}

		resultSet.close();

		return listStations;
	}

	/**
	 * Query station's info from database.
	 * @param id which is station's id.
	 * @return a Station entity or null
	 * @throws ClassNotFoundException if no class would be found
	 * @throws SQLException if there was an error with SQL query execution
	 */
	public Station queryStationInfo(int id) throws ClassNotFoundException, SQLException {
		String query = "select * from station where id=" + id;
		ResultSet resultSet = statement.executeQuery(query);

		if (resultSet.next()) {
			String name = resultSet.getString("name");
			String address = resultSet.getString("address");
			return new Station(id, name, address);
		} else {
			return null;
		}

	}
}
