package hust.tkxdpm.entities;

/**
 * @author SpQuyt
 * @date Oct 23, 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public class Card {

	private String cardId;
	private String cardCode;
	private Customer customer;
	private double balance;
	private Station checkInStation;

	/**
	 * @param cardId
	 * @param cardCode
	 * @param customer
	 * @param balance
	 * @param checkInStation
	 */
	public Card(String cardId, String cardCode, Customer customer, double balance, Station checkInStation) {
		super();
		this.cardId = cardId;
		this.setCardCode(cardCode);
		this.customer = customer;
		this.balance = balance;
		this.checkInStation = checkInStation;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public Station getCheckInStation() {
		return checkInStation;
	}

	public void setCheckInStation(Station checkInStation) {
		this.checkInStation = checkInStation;
	}

	public String getCardCode() {
		return cardCode;
	}

	public void setCardCode(String cardCode) {
		this.cardCode = cardCode;
	}

}
