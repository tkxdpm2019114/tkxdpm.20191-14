package hust.tkxdpm.entities;

/**
 * @author SpQuyt
 * @date Oct 23, 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public class OneWayTicket extends Ticket {

	private Station checkOutStation;
	private Station defaultInStation;
	private Station defaultOutStation;

	public OneWayTicket(String ticketId, String ticketCode, Customer customer, Station checkInStation, Station checkOutStation,
			Station defaultInStation, Station defaultOutStation) {
		super(ticketId, ticketCode, customer, checkInStation);
		this.setCheckOutStation(checkOutStation);
		this.defaultInStation = defaultInStation;
		this.defaultOutStation = defaultOutStation;
	}

	public Station getDefaultInStation() {
		return defaultInStation;
	}

	public void setDefaultInStation(Station defaultInStation) {
		this.defaultInStation = defaultInStation;
	}

	public Station getDefaultOutStation() {
		return defaultOutStation;
	}

	public void setDefaultOutStation(Station defaultOutStation) {
		this.defaultOutStation = defaultOutStation;
	}

	public Station getCheckOutStation() {
		return checkOutStation;
	}

	public void setCheckOutStation(Station checkOutStation) {
		this.checkOutStation = checkOutStation;
	}

}
