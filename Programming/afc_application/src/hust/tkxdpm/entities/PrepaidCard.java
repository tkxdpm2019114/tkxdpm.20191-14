/**
 * 
 */
package hust.tkxdpm.entities;

/**
 * @author SpQuyt
 * @date 23 Dec 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public class PrepaidCard extends Card {

	/**
	 * @param cardId
	 * @param customer
	 * @param balance
	 * @param checkInStation
	 */
	public PrepaidCard(String cardId, String cardCode, Customer customer, double balance, Station checkInStation) {
		super(cardId, cardCode, customer, balance, checkInStation);
	}

}
