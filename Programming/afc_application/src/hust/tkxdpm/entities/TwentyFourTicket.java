package hust.tkxdpm.entities;

import java.sql.Timestamp;

/**
 * @author SpQuyt
 * @date Oct 23, 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public class TwentyFourTicket extends Ticket {

	private Timestamp expiredTime;
	private boolean isExpired = false;
    
	public TwentyFourTicket(String ticketId, String ticketcode, Customer customer, Station checkInStation,Timestamp expiredTime) {
		super(ticketId, ticketcode, customer, checkInStation);
	    this.expiredTime = expiredTime;
	}

	
	public Timestamp getExpiredTime() {
		return expiredTime;
	}

	public void setExpiredTime(Timestamp expiredTime) {
		this.expiredTime = expiredTime;
	}

	public boolean isExpired() {
		return isExpired;
	}

	public void setExpired(boolean isExpired) {
		this.isExpired = isExpired;
	}

}
