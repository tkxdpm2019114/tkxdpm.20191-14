package hust.tkxdpm.entities;

import hust.tkxdpm.controllers.TicketController;

/**
 * @author SpQuyt
 * @date Oct 23, 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public class Ticket {

	private String ticketId;
	private String ticketCode;
	private Customer customer;
	private Station checkInStation;
	private TicketController ticketController;
	private Station station;

	public Ticket(String ticketId, String ticketCode, Customer customer, Station checkInStation) {
		this.ticketId = ticketId;
		this.ticketCode = ticketCode;
		this.customer = customer;
		this.checkInStation = checkInStation;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getTicketCode() {
		return ticketCode;
	}

	public void setTicketCode(String ticketCode) {
		this.ticketCode = ticketCode;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Station getCheckInStation() {
		return checkInStation;
	}

	public void setCheckInStation(Station checkInStation) {
		this.checkInStation = checkInStation;
	}

	public TicketController getTicketController() {
		return ticketController;
	}

	public void setTicketController(TicketController ticketController) {
		this.ticketController = ticketController;
	}

	public Station getStation() {
		return station;
	}

	public void setStation(Station station) {
		this.station = station;
	}

}
