package hust.tkxdpm.controllers;

import java.sql.*;
import java.util.ArrayList;

import hust.tkxdpm.boundaries.AppUI;
import hust.tkxdpm.dao.*;
import hust.tkxdpm.entities.OneWayTicket;
import hust.tkxdpm.entities.Station;
import hust.tkxdpm.entities.Ticket;
import hust.tkxdpm.helpers.*;

/**
 * @author SpQuyt
 * @date Oct 25, 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public class StationController {
	private StationDAO stationDao = new StationDAO();
	
	/**
	 * Method to request getting all the stations from database.
	 * 
	 * @return listStations a list of stations
	 */
	public ArrayList<Station> getAllStations() {
		ArrayList<Station> listStations = new ArrayList<Station>();

		try {
			listStations = this.stationDao.queryAllStations();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return listStations;
	}
	
	/**
	 * Specifically for check-in One-Way Ticket. Check if current station is valid
	 * for customer to get in?
	 * 
	 * @param ticket Ticket entity
	 * @return true if valid; false if invalid
	 */
	public boolean checkValidInStation(Ticket ticket) {
		int beginIndex = -1;
		int endIndex = -1;
		String defaultInStationName = ((OneWayTicket) ticket).getDefaultInStation().getName();
		String defaultOutStationName = ((OneWayTicket) ticket).getDefaultOutStation().getName();

		// Get the index of defaultInStation and defaultOutStation using beginIndex and
		// endIndex
		for (Station station : AppUI.listStations) {
			if (station.getName().equals(defaultInStationName)) {
				beginIndex = AppUI.listStations.indexOf(station);
			}
			if (station.getName().equals(defaultOutStationName)) {
				endIndex = AppUI.listStations.indexOf(station);
			}
		}

		// If customer is going from the higher Id to lower Id, we have to swap.
		// Because in database we only save the road from lower Id to higher Id
		if (beginIndex > endIndex) {
			int temp;
			temp = beginIndex;
			beginIndex = endIndex;
			endIndex = temp;
		}

		// If currentStation's name matches with a station's name in list from
		// beginIndex to endIndex, break and return true
		for (int i = beginIndex; i <= endIndex; i++) {
			if (AppUI.currentStation.getName().equals(AppUI.listStations.get(i).getName())) {
				return true;
			}
		}
		// Else, return false
		return false;
	}
}
