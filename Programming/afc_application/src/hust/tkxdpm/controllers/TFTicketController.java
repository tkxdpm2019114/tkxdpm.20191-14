package hust.tkxdpm.controllers;

import java.sql.SQLException;
import java.util.ArrayList;

import hust.tkxdpm.boundaries.AppUI;
import hust.tkxdpm.dao.OtherQuery;
import hust.tkxdpm.dao.TicketDAO;
import hust.tkxdpm.entities.OneWayTicket;
import hust.tkxdpm.entities.Station;
import hust.tkxdpm.entities.Ticket;
import hust.tkxdpm.entities.TwentyFourTicket;
import hust.tkxdpm.helpers.MyEnum.TicketEnum;
import hust.tkxdpm.strategy.IPriceStrategy;
import hust.tkxdpm.strategy.PriceStrategy;

/**
 * @author SpQuyt
 * @date Nov 18, 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public class TFTicketController implements TicketController {
	private Ticket ticket;

	public Ticket getTicketInfo() {
		return this.ticket;
	}

	@Override
	public TicketEnum checkIn(Ticket ticket) throws ClassNotFoundException, SQLException {
		// If the ticket has not been used for checked-in yet
		if (ticket.getCheckInStation() == null) {

			// If this TwentyFour Ticket has not been expired
			long expiredTime = ((TwentyFourTicket) ticket).getExpiredTime().getTime();
			long currentTime = System.currentTimeMillis();
			if (expiredTime > currentTime) {
				this.ticketDao.updateInStationTFTicket(ticket.getTicketCode(), AppUI.currentStation);
				this.queryTicketInfo(ticket.getTicketCode());
				return TicketEnum.SUCCESSIN;
			} else {
				return TicketEnum.OUTDATEDTICKET;
			}
		} else {
			return TicketEnum.ALREADYCHECKEDIN;
		}
	}

	@Override
	public TicketEnum checkOut(Ticket ticket) throws ClassNotFoundException, SQLException {
		// If the ticket has already been used for checked-in yet
		if (ticket.getCheckInStation() != null) {

			// If this TwentyFour Ticket has not been expired
			long expiredTime = ((TwentyFourTicket) ticket).getExpiredTime().getTime();
			long currentTime = System.currentTimeMillis();
			if (expiredTime > currentTime) {
				this.ticketDao.updateOutStationTFTicket(ticket.getTicketCode());
				this.otherQuery.insertNewHistoryPath(ticket.getCheckInStation(), AppUI.currentStation, ticket.getCustomer());
				this.queryTicketInfo(ticket.getTicketCode());
				return TicketEnum.SUCCESSOUT;
			} else {
				return TicketEnum.OUTDATEDTICKET;
			}
		} else {
			return TicketEnum.NOTCHECKEDINYET;
		}
	}

	@Override
	public void queryTicketInfo(String ticketcode) throws ClassNotFoundException, SQLException, NullPointerException {
		TwentyFourTicket resultTF = this.ticketDao.queryTFTicketInfo(ticketcode);
		ticket = resultTF;
	}

	@Override
	public TicketEnum checkOut(Ticket ticket, IPriceStrategy option) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		return null;
	}
}
