package hust.tkxdpm.controllers;

import java.sql.SQLException;
import java.util.ArrayList;

import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.recognizer.TicketRecognizer;
import hust.tkxdpm.dao.*;
import hust.tkxdpm.entities.*;
import hust.tkxdpm.helpers.MyEnum.TicketEnum;
import hust.tkxdpm.strategy.IPriceStrategy;
import hust.tkxdpm.strategy.PriceCalculateByInOutStation;
import hust.tkxdpm.strategy.PriceStrategy;

/**
 * @author SpQuyt
 * @date Oct 23, 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public interface TicketController {
	public TicketDAO ticketDao = new TicketDAO();
	public OtherQuery otherQuery = new OtherQuery();
	public PriceStrategy strategy = new PriceStrategy();

	/**
	 * Main method to process logical behaviors of check-in ticket. Return a
	 * TicketEnum to determine which dialog will be displayed.
	 * 
	 * @param ticket Entity
	 * @return TicketEnum
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public abstract TicketEnum checkIn(Ticket ticket) throws ClassNotFoundException, SQLException;

	/**
	 * Main method to process logical behaviors of check-out ticket. Return a
	 * TicketEnum to determine which dialog will be displayed.
	 * 
	 * @param ticket Entity
	 * @return TicketEnum
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public abstract TicketEnum checkOut(Ticket ticket) throws ClassNotFoundException, SQLException;
	
	/**
	 * Main method to process logical behaviors of check-out ticket. Return a
	 * TicketEnum to determine which dialog will be displayed.
	 * 
	 * @param ticket Entity
	 * @param option price's strategy
	 * @return TicketEnum
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public abstract TicketEnum checkOut(Ticket ticket, IPriceStrategy option) throws ClassNotFoundException, SQLException;

	/**
	 * Get ticket's information
	 * 
	 * @param ticketcode which has 14 digits
	 * @throws ClassNotFoundException if no class would be found
	 * @throws SQLException           if there was an error with SQL query execution
	 * @throws NullPointerException   if some NULL errors occur
	 */
	public abstract void queryTicketInfo(String ticketcode) throws ClassNotFoundException, SQLException, NullPointerException;
}
