/**
 * 
 */
package hust.tkxdpm.controllers;

import java.sql.SQLException;

import hust.soict.se.customexception.InvalidIDException;
import hust.tkxdpm.boundaries.*;
import hust.tkxdpm.dao.*;
import hust.tkxdpm.entities.*;
import hust.tkxdpm.helpers.MyEnum.CardEnum;
import hust.tkxdpm.strategy.IPriceStrategy;
import hust.tkxdpm.helpers.MyFormatter;

/**
 * @author SpQuyt
 * @date 24 Dec 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public class PPCardController implements CardController {
	private Card card;
	private double totalAmount;

	public Card getTicketInfo() {
		return this.card;
	}

	public double getTotalAmount() {
		return this.totalAmount;
	}

	@Override
	public CardEnum checkIn(Card card) {
		try {
			/** If the card has not been used for checked-in */
			if (card.getCheckInStation() == null) {
				this.cardDao.updateInStationPPCard(card.getCardCode(), AppUI.currentStation);
				this.queryCardInfo(card.getCardCode());
				return CardEnum.SUCCESSIN;
			} else {
				return CardEnum.ALREADYCHECKEDIN;
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return CardEnum.CODENOTINDATABASE;
		} catch (SQLException e) {
			e.printStackTrace();
			return CardEnum.CODENOTINDATABASE;
		} catch (NullPointerException e) {
			e.printStackTrace();
			return CardEnum.CODENOTINDATABASE;
		}
	}

	@Override
	public CardEnum checkOut(Card card, IPriceStrategy option) {
		// Set the price strategy option
		this.strategy.setPriceStrategy(option);

		try {
			/** If the card has been used for checked-in */
			if (card.getCheckInStation() != null) {

				/** Calculate distance and price of the whole trip */
				this.totalAmount = this.strategy.calculatePrice(card.getCheckInStation(), AppUI.currentStation);

				/** If the balance is enough */
				if (card.getBalance() >= this.totalAmount) {
					double balanceOut = MyFormatter.formatDoubleDecimalDigits(card.getBalance() - this.totalAmount);
					this.cardDao.updateOutStationPPCard(card.getCardCode(), balanceOut);
					this.otherQuery.insertNewHistoryPath(card.getCheckInStation(), AppUI.currentStation, card.getCustomer());
					this.queryCardInfo(card.getCardCode());
					return CardEnum.SUCCESSOUT;
				} else {
					return CardEnum.BALANCENOTENOUGH;
				}

			} else {
				return CardEnum.NOTCHECKEDINYET;
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return CardEnum.CODENOTINDATABASE;
		} catch (SQLException e) {
			e.printStackTrace();
			return CardEnum.CODENOTINDATABASE;
		} catch (NullPointerException e) {
			e.printStackTrace();
			return CardEnum.CODENOTINDATABASE;
		}
	}

	@Override
	public void queryCardInfo(String cardcode) throws ClassNotFoundException, SQLException {
		PrepaidCard resultPP = (PrepaidCard) this.cardDao.queryPPCardInfo(cardcode);
		card = resultPP;
	}

	@Override
	public CardEnum checkOut(Card card) {
		return null;
	}
}
