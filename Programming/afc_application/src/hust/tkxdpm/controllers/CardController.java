package hust.tkxdpm.controllers;

import java.sql.SQLException;
import java.util.ArrayList;

import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.scanner.CardScanner;
import hust.tkxdpm.boundaries.*;
import hust.tkxdpm.dao.*;
import hust.tkxdpm.entities.*;
import hust.tkxdpm.helpers.MyFormatter;
import hust.tkxdpm.helpers.MyEnum.CardEnum;
import hust.tkxdpm.strategy.*;

/**
 * @author SpQuyt
 * @date Oct 23, 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public interface CardController {
	public CardDAO cardDao = new CardDAO();
	public OtherQuery otherQuery = new OtherQuery();
	public PriceStrategy strategy = new PriceStrategy();

	/**
	 * Main method to process logical behaviors of check-in card. Return a CardEnum
	 * to determine which dialog will be displayed.
	 * 
	 * @param card Entity.
	 * @return CardEnum
	 */
	public abstract CardEnum checkIn(Card card);

	/**
	 * Main method to process logical behaviors of check-out card. Return a CardEnum
	 * to determine which dialog will be displayed.
	 * 
	 * @param card Entity.
	 * @return CardEnum
	 */
	public abstract CardEnum checkOut(Card card);
	
	/**
	 * Main method to process logical behaviors of check-out card. Return a CardEnum
	 * to determine which dialog will be displayed.
	 * 
	 * @param card Entity.
	 * @param option price's strategy
	 * @return CardEnum
	 */
	public abstract CardEnum checkOut(Card card, IPriceStrategy option);

	/**
	 * Request getting Card's info from database.
	 * 
	 * @param cardcode which has 14 digits.
	 * @throws ClassNotFoundException if no class would be found
	 * @throws SQLException           if there was an error with SQL query execution
	 */
	public abstract void queryCardInfo(String cardcode) throws ClassNotFoundException, SQLException;
}
