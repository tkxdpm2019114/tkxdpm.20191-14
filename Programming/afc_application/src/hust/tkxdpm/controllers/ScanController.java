/**
 * 
 */
package hust.tkxdpm.controllers;

import hust.soict.se.recognizer.TicketRecognizer;
import hust.soict.se.scanner.CardScanner;
import hust.tkxdpm.dao.CardDAO;
import hust.tkxdpm.dao.TicketDAO;
import hust.tkxdpm.entities.Card;
import hust.tkxdpm.entities.OneWayTicket;
import hust.tkxdpm.entities.PrepaidCard;
import hust.tkxdpm.entities.Ticket;
import hust.tkxdpm.entities.TwentyFourTicket;

/**
 * @author SpQuyt
 * @date 23 Dec 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public class ScanController {
	private TicketRecognizer tr = TicketRecognizer.getInstance();
	private CardScanner cs = CardScanner.getInstance();
	private TicketDAO ticketDao = new TicketDAO();
	private CardDAO cardDao = new CardDAO();
	
	/**
	 * Help converting barcode to a ticket Entity.
	 * 
	 * @param barcode String.
	 * @return Ticket
	 */
	public Ticket convertBarcodeToTicket(String barcode) {
		Ticket result = null;
		try {
			String ticketcode = tr.process(barcode);
			System.out.println("Check-in ticket: " + ticketcode);

			OneWayTicket resultOW = this.ticketDao.queryOWTicketInfo(ticketcode);
			TwentyFourTicket resultTF = this.ticketDao.queryTFTicketInfo(ticketcode);
			if (resultOW == null) {
				if (resultTF == null) {
					result = null;
				} else {
					result = resultTF;
				}
			} else {
				result = resultOW;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * Help converting barcode to a card Entity.
	 * 
	 * @param barcode String.
	 * @return Card
	 */
	public Card convertBarcodeToCard(String barcode) {
		Card result = null;
		try {
			String cardcode = cs.process(barcode);
			System.out.println("Check-in card: " + cardcode);
			
			PrepaidCard resultPP = this.cardDao.queryPPCardInfo(cardcode);
			result = resultPP;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
