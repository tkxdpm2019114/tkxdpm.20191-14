package hust.tkxdpm.controllers;

import java.sql.SQLException;
import java.util.ArrayList;

import hust.tkxdpm.boundaries.AppUI;
import hust.tkxdpm.dao.OtherQuery;
import hust.tkxdpm.dao.TicketDAO;
import hust.tkxdpm.entities.OneWayTicket;
import hust.tkxdpm.entities.Station;
import hust.tkxdpm.entities.Ticket;
import hust.tkxdpm.entities.TwentyFourTicket;
import hust.tkxdpm.helpers.MyEnum.TicketEnum;
import hust.tkxdpm.strategy.*;

/**
 * @author SpQuyt
 * @date Nov 18, 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public class OWTicketController implements TicketController {
	private Ticket ticket;
	private StationController stationController = new StationController();

	public Ticket getTicketInfo() {
		return this.ticket;
	}

	@Override
	public TicketEnum checkIn(Ticket ticket) throws ClassNotFoundException, SQLException {
		// If the ticket has not been used for checked-in yet
		if (ticket.getCheckInStation() == null) {

			// If currentStation is suitable for check-in using this OWTicket
			if (this.stationController.checkValidInStation(ticket)) {
				this.ticketDao.updateInStationOWTicket(ticket.getTicketCode(), AppUI.currentStation);
				this.queryTicketInfo(ticket.getTicketCode());
				return TicketEnum.SUCCESSIN;
			} else {
				return TicketEnum.NOTALLOWEDINSTATION;
			}
		} else {
			return TicketEnum.ALREADYCHECKEDIN;
		}
	}

	@Override
	public TicketEnum checkOut(Ticket ticket, IPriceStrategy option) throws ClassNotFoundException, SQLException {
		// Set the price strategy option
		this.strategy.setPriceStrategy(option);

		// If the ticket has already been used for checked-in yet
		if (ticket.getCheckInStation() != null) {

			// If currentStation is the checkedInStation
			if (!ticket.getCheckInStation().getName().equals(AppUI.currentStation.getName())) {

				// If this OWTicket has not been used for checked-out ?
				if (((OneWayTicket) ticket).getCheckOutStation() == null) {

					// Calculate distance and price of the trip
					double totalAmount = this.strategy.calculatePrice(ticket.getCheckInStation(), AppUI.currentStation);

					// Calculate distance and price of default trip
					double defaultAmount = this.strategy.calculatePrice(((OneWayTicket) ticket).getDefaultInStation(),
							((OneWayTicket) ticket).getDefaultOutStation());

					// If the balance is enough
					if (defaultAmount >= totalAmount) {
						this.ticketDao.updateOutStationOWTicket(ticket.getTicketCode(), AppUI.currentStation);
						this.otherQuery.insertNewHistoryPath(ticket.getCheckInStation(), AppUI.currentStation,
								ticket.getCustomer());
						this.queryTicketInfo(ticket.getTicketCode());
						return TicketEnum.SUCCESSOUT;
					} else {
						return TicketEnum.BALANCENOTENOUGH;
					}
				} else {
					return TicketEnum.OUTDATEDTICKET;
				}
			} else {
				return TicketEnum.NOTALLOWEDOUTSTATION;
			}
		} else {
			return TicketEnum.NOTCHECKEDINYET;
		}
	}

	@Override
	public void queryTicketInfo(String ticketcode) throws ClassNotFoundException, SQLException, NullPointerException {
		OneWayTicket resultOW = this.ticketDao.queryOWTicketInfo(ticketcode);
		ticket = resultOW;
	}

	@Override
	public TicketEnum checkOut(Ticket ticket) throws ClassNotFoundException, SQLException {
		return null;
	}

}
