/**
 * 
 */
package hust.tkxdpm.strategy;

import java.sql.SQLException;

import hust.tkxdpm.boundaries.AppUI;
import hust.tkxdpm.dao.OtherQuery;
import hust.tkxdpm.entities.Station;
import hust.tkxdpm.helpers.MyFormatter;

/**
 * @author SpQuyt
 * @date 23 Dec 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public class PriceCalculateByInOutStation implements IPriceStrategy {
	private double distance = 0;
	private OtherQuery otherQuery = new OtherQuery();

	/**
	 * Calculate the price of whole trip.
	 * 
	 * @param distance the distance of the whole trip
	 * @return totalAmount price of the whole trip
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	@Override
	public double calculatePrice(Station inStation, Station outStation) throws ClassNotFoundException, SQLException {
		double totalAmount = 0;
		double baseFare = 1.9;
		double baseAddition = 0.4;
		this.calculateDistance(inStation, outStation);

		if (distance <= 5) {
			totalAmount = baseFare;
		} else {
			distance = distance - 5;
			totalAmount += baseFare;
			while (distance > 0) {
				totalAmount += baseAddition;
				distance -= 2;
			}
		}
		return MyFormatter.formatDoubleDecimalDigits(totalAmount);
	}
	
	/**
	 * Calculate the distance of whole trip. Because we only save the distance of 2
	 * each adjacent stations in database, like (19,20) or (20,21) So if we want to
	 * calculate the distance of (20,24) we have to query all (20,21), (21,22),
	 * (22,23), (23,24)
	 * 
	 * @param inStation    the station which the customer is getting in
	 * @param outStation   the station which the customer is getting out
	 * @return totalDistance distance of the whole trip
	 */
	private void calculateDistance(Station inStation, Station outStation)
			throws ClassNotFoundException, SQLException {

		double totalDistance = 0;
		int beginIndex = -1;
		int endIndex = -1;

		// Get the index of inStation and outStation using beginIndex and endIndex
		for (Station station : AppUI.listStations) {
			if (station.getName().equals(inStation.getName())) {
				beginIndex = AppUI.listStations.indexOf(station);
			}
			if (station.getName().equals(outStation.getName())) {
				endIndex = AppUI.listStations.indexOf(station);
			}
		}

		// If customer is going from the higher Id to lower Id, we have to swap.
		// Because in database we only save the road from lower Id to higher Id
		if (beginIndex > endIndex) {
			int temp;
			temp = beginIndex;
			beginIndex = endIndex;
			endIndex = temp;
		}

		// Query each 2 adjacent stations in database, we will add to totalDistance
		// after
		// each loop
		// The code not in comment is used for the whole app
		// The code in comment is used for testing the main()

		for (int i = beginIndex; i < endIndex; i++) {
			totalDistance = totalDistance
					+ this.otherQuery.queryDistance(AppUI.listStations.get(i), AppUI.listStations.get(i + 1));
		}

		this.distance = MyFormatter.formatDoubleDecimalDigits(totalDistance);
	}

}
