/**
 * 
 */
package hust.tkxdpm.strategy;

import java.sql.SQLException;

import javax.swing.JOptionPane;

import hust.tkxdpm.entities.Station;

/**
 * @author SpQuyt
 * @date 23 Dec 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public class PriceStrategy {
	private IPriceStrategy strategy;
	
	public void setPriceStrategy(IPriceStrategy newStrategy ) {
		this.strategy = newStrategy;
	}
	
	public double calculatePrice(Station inStation, Station outStation) {
		double result = 0;
		try {
			result = this.strategy.calculatePrice(inStation, outStation);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "ClassNotFoundException!");
		} catch (SQLException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "SQLException!");
		}
		return result;
	}
}
