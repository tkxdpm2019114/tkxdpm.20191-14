/**
 * 
 */
package hust.tkxdpm.strategy;

import java.sql.SQLException;

import hust.tkxdpm.entities.Station;

/**
 * @author SpQuyt
 * @date 23 Dec 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public interface IPriceStrategy {
	/**
	 * @param inStation
	 * @param outStation
	 * @return
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	public double calculatePrice(Station inStation, Station outStation) throws ClassNotFoundException, SQLException;
}
