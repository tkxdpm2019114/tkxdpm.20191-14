package hust.tkxdpm.helpers;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

/**
 * @author SpQuyt
 * @date Nov 19, 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public class MyFormatter {
	public static String formatDateTime(Timestamp timestamp) {
		return new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss").format(timestamp);
	}
	
	public static String formatDateTime(long timestamp) {
		return new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss").format(timestamp);
	}
	
	public static Double formatDoubleDecimalDigits(double number) {
		return Double.valueOf(new DecimalFormat("##.#").format(number));
	}
}
