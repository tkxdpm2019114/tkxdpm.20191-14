package hust.tkxdpm.helpers;

import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;

/**
 * @author SpQuyt
 * @date Oct 23, 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public class PointLayout {
	
	public static int startXPoints(int frameWidth, int frameHeight) {
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int tempX = (int) (screen.getWidth()/2);
		
		return (tempX - (int) (frameWidth/2));
	}
	
	public static int startYPoints(int frameWidth, int frameHeight) {
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int tempY = (int) (screen.getHeight()/2);
		
		int taskbarheight = (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight() 
			    - GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().getHeight());
		
		return (tempY - (int) ((frameHeight + taskbarheight)/2));
	}

}
