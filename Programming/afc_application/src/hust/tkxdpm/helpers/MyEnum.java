package hust.tkxdpm.helpers;

/**
 * @author SpQuyt
 * @date Nov 18, 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public class MyEnum {
	public static enum CardEnum {
		SUCCESSIN, SUCCESSOUT, ALREADYCHECKEDIN, NOTCHECKEDINYET, CODENOTINDATABASE, BALANCENOTENOUGH,
	};

	public static enum TicketEnum {
		SUCCESSIN, SUCCESSOUT, ALREADYCHECKEDIN, NOTCHECKEDINYET, CODENOTINDATABASE, BALANCENOTENOUGH,
		NOTALLOWEDINSTATION, NOTALLOWEDOUTSTATION, OUTDATEDTICKET
	};
}
