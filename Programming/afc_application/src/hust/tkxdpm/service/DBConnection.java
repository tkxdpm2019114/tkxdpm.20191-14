package hust.tkxdpm.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author SpQuyt
 * @date Nov 6, 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public class DBConnection {
	private Statement statement;
// 	private static String host =
// 	"localhost/afc?useLegacyDatetimeCode=false&serverTimezone=UTC";
// 	private static String user = "root";
// 	private static String password = "";
	private static String host = "127.0.0.1:3306/afc";
	private static String user = "SpQuyt";
	private static String password = "Megamanx8!";
//	private static String user = "theblue";
//	private static String password = "abc123";

	public DBConnection() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection connect = DriverManager.getConnection("jdbc:mysql://" + host, user, password);
			setStatement(connect.createStatement());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public Statement getStatement() {
		return this.statement;
	}

	public void setStatement(Statement statement) {
		this.statement = statement;
	}
}
