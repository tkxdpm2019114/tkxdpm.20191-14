/**
 * 
 */
package hust.tkxdpm.app;

import hust.tkxdpm.boundaries.AppUI;

/**
 * @author SpQuyt
 * @date Nov 18, 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public class Main {

	public static void main(String[] args) {
		new AppUI();
	}

}
