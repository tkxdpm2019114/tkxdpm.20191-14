/**
 * 
 */
package hust.tkxdpm.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import hust.tkxdpm.boundaries.AppUI;
import hust.tkxdpm.controllers.OWTicketController;
import hust.tkxdpm.controllers.ScanController;
import hust.tkxdpm.controllers.StationController;
import hust.tkxdpm.controllers.TicketController;
import hust.tkxdpm.entities.Station;
import hust.tkxdpm.helpers.MyEnum.TicketEnum;

/**
 * @author SpQuyt
 * @date 5 Dec 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
class TestTicketControllerOut {
	private StationController stationControl = new StationController();
	private ArrayList<Station> listStations = stationControl.getAllStations();
	
	private ScanController sc = new ScanController();
	private OWTicketController owtc = new OWTicketController();
	
	private Station currentStation1 = new Station(19, "Saint-Lazare", "Paris"),
			currentStation2 = new Station(20, "Madeleine", "Paris"),
			currentStation3 = new Station(21, "Pyramides", "Paris"),
			currentStation4 = new Station(22, "Chatelet", "Paris"),
			currentStation5 = new Station(23, "Gare de Lyon", "Paris"),
			currentStation6 = new Station(24, "Bercy", "Paris"),
			currentStation7 = new Station(25, "Cour Saint-Emilion", "Paris"),
			currentStation8 = new Station(26, "Bibliothque Francois Mitterrand", "Paris"),
			currentStation9 = new Station(27, "Olympiades", "Paris");


//	/**
//	 * Test method for
//	 * {@link hust.tkxdpm.controllers.TicketController#checkTicketOut(java.lang.String, java.util.ArrayList, hust.tkxdpm.entities.Station)}.
//	 */
//	@Test
//	void testCheckTicketOutaaaaaaaa() {
//		new AppUI();
//		AppUI.currentStation = currentStation6;
//		TicketEnum result = tc.checkOut("aaaaaaaa");
//
//		assertEquals(TicketEnum.NOTCHECKEDINYET, result);
//	}
//	
//	/**
//	 * Test method for
//	 * {@link hust.tkxdpm.controllers.TicketController#checkTicketOut(java.lang.String, java.util.ArrayList, hust.tkxdpm.entities.Station)}.
//	 */
//	@Test
//	void testCheckTicketOutbbbbbbbb() {
//		new AppUI();
//		AppUI.currentStation = currentStation5;
//		TicketEnum result = tc.checkOut("bbbbbbbb");
//
//		assertEquals(TicketEnum.NOTALLOWEDOUTSTATION, result);
//	}
//	
//	/**
//	 * Test method for
//	 * {@link hust.tkxdpm.controllers.TicketController#checkTicketOut(java.lang.String, java.util.ArrayList, hust.tkxdpm.entities.Station)}.
//	 */
//	@Test
//	void testCheckTicketOutaaaaaaaa2() {
//		new AppUI();
//		AppUI.currentStation = currentStation5;
//		TicketEnum result = tc.checkOut("aaaaaaaa");
//
//		assertEquals(TicketEnum.NOTCHECKEDINYET, result);
//	}
//	
//	/**
//	 * Test method for
//	 * {@link hust.tkxdpm.controllers.TicketController#checkTicketOut(java.lang.String, java.util.ArrayList, hust.tkxdpm.entities.Station)}.
//	 */
//	@Test
//	void testCheckTicketOutaaaaaaaa3() {
//		new AppUI();
//		AppUI.currentStation = currentStation1;
//		TicketEnum result = tc.checkOut("aaaaaaaa");
//
//		assertEquals(TicketEnum.NOTCHECKEDINYET, result);
//	}
//	
//	/**
//	 * Test method for
//	 * {@link hust.tkxdpm.controllers.TicketController#checkTicketOut(java.lang.String, java.util.ArrayList, hust.tkxdpm.entities.Station)}.
//	 */
//	@Test
//	void testCheckTicketOutdddddddd() {
//		new AppUI();
//		AppUI.currentStation = currentStation3;
//		TicketEnum result = tc.checkOut("dddddddd");
//
//		assertEquals(TicketEnum.OUTDATEDTICKET, result);
//	}
//	
//	/**
//	 * Test method for
//	 * {@link hust.tkxdpm.controllers.TicketController#checkTicketOut(java.lang.String, java.util.ArrayList, hust.tkxdpm.entities.Station)}.
//	 */
//	@Test
//	void testCheckTicketOutcccccccc() {
//		new AppUI();
//		AppUI.currentStation = currentStation1;
//		TicketEnum result = tc.checkOut("cccccccc");
//
//		assertEquals(TicketEnum.NOTCHECKEDINYET, result);
//	}
//	
//	/**
//	 * Test method for
//	 * {@link hust.tkxdpm.controllers.TicketController#checkTicketOut(java.lang.String, java.util.ArrayList, hust.tkxdpm.entities.Station)}.
//	 */
//	@Test
//	void testCheckTicketOuteeeeeeee() {
//		new AppUI();
//		AppUI.currentStation = currentStation1;
//		TicketEnum result = tc.checkOut("eeeeeeee");
//
//		assertEquals(TicketEnum.NOTCHECKEDINYET, result);
//	}
//	
//	/**
//	 * Test method for
//	 * {@link hust.tkxdpm.controllers.TicketController#checkTicketOut(java.lang.String, java.util.ArrayList, hust.tkxdpm.entities.Station)}.
//	 */
//	@Test
//	void testCheckTicketOutabcddd12() {
//		new AppUI();
//		AppUI.currentStation = currentStation1;
//		TicketEnum result = tc.checkOut("abcddd12");
//
//		assertEquals(TicketEnum.CODENOTINDATABASE, result);
//	}
//	
//	/**
//	 * Test method for
//	 * {@link hust.tkxdpm.controllers.TicketController#checkTicketOut(java.lang.String, java.util.ArrayList, hust.tkxdpm.entities.Station)}.
//	 */
//	@Test
//	void testCheckTicketOutaabbccdd() {
//		new AppUI();
//		AppUI.currentStation = currentStation1;
//		TicketEnum result = tc.checkOut("aabbccdd");
//
//		assertEquals(TicketEnum.CODENOTINDATABASE, result);
//	}
//	
//	/**
//	 * Test method for
//	 * {@link hust.tkxdpm.controllers.TicketController#checkTicketOut(java.lang.String, java.util.ArrayList, hust.tkxdpm.entities.Station)}.
//	 */
//	@Test
//	void testCheckTicketOutddab() {
//		new AppUI();
//		AppUI.currentStation = currentStation1;
//		TicketEnum result = tc.checkOut("ddab");
//
//		assertEquals(TicketEnum.CODENOTINDATABASE, result);
//	}

}
