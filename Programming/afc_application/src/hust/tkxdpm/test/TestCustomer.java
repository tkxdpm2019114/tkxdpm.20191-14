package hust.tkxdpm.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import hust.tkxdpm.entities.Customer;
import junit.framework.TestSuite;

/**
 * 
 */

/**
 * @author SpQuyt
 * @date Nov 7, 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
class TestCustomer {
	
	public TestCustomer() {  
        super();  
    }

	@Test
	void test() {
		fail("Not yet implemented");
	}

	public void testCreateCustomer() {  
        System.out.println("Begin testCreateCustomer()");  
        Customer c = new Customer(1, "Truong Anh Quoc", "0989652445");
        assertEquals("Truong Anh Quoc", c.getName());  
        System.out.println("End testCreateCustomer()");  
    } 
	
	public static void main(String[] args) {
		junit.textui.TestRunner.run(new TestSuite(TestCustomer.class));
	}
}
