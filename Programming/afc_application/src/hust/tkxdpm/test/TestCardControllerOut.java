/**
 * 
 */
package hust.tkxdpm.test;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import hust.tkxdpm.boundaries.AppUI;
import hust.tkxdpm.controllers.CardController;
import hust.tkxdpm.controllers.StationController;
import hust.tkxdpm.entities.Station;
import hust.tkxdpm.helpers.MyEnum.CardEnum;

/**
 * @author SpQuyt
 * @date 5 Dec 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
class TestCardControllerOut {
	private StationController stationControl = new StationController();
	private ArrayList<Station> listStations = stationControl.getAllStations();
	private CardController cc = CardController.getInstance();
	private Station currentStation1 = new Station(19, "Saint-Lazare", "Paris"),
			currentStation2 = new Station(20, "Madeleine", "Paris"),
			currentStation3 = new Station(21, "Pyramides", "Paris"),
			currentStation4 = new Station(22, "Chatelet", "Paris"),
			currentStation5 = new Station(23, "Gare de Lyon", "Paris"),
			currentStation6 = new Station(24, "Bercy", "Paris"),
			currentStation7 = new Station(25, "Cour Saint-Emilion", "Paris"),
			currentStation8 = new Station(26, "Bibliothque Francois Mitterrand", "Paris"),
			currentStation9 = new Station(27, "Olympiades", "Paris");


	/**
	 * Test method for
	 * {@link hust.tkxdpm.controllers.CardController#checkOut(java.lang.String)}.
	 */
	@Test
	void testCheckCardOutCCCCCCCC() {
		new AppUI();
		AppUI.currentStation = currentStation2;
		CardEnum result = cc.checkOut("CCCCCCCC");
		assertEquals(CardEnum.SUCCESSOUT, result);
	}
	
	/**
	 * Test method for
	 * {@link hust.tkxdpm.controllers.CardController#checkOut(java.lang.String)}.
	 */
	@Test
	void testCheckCardOutBBBBBBBB() {
		new AppUI();
		AppUI.currentStation = currentStation5;
		CardEnum result = cc.checkOut("BBBBBBBB");
		assertEquals(CardEnum.BALANCENOTENOUGH, result);
	}
	
	/**
	 * Test method for
	 * {@link hust.tkxdpm.controllers.CardController#checkOut(java.lang.String)}.
	 */
	@Test
	void testCheckCardOutAAAAAAAA() {
		new AppUI();
		AppUI.currentStation = currentStation2;
		CardEnum result = cc.checkOut("AAAAAAAA");
		assertEquals(CardEnum.NOTCHECKEDINYET, result);
	}
	
	/**
	 * Test method for
	 * {@link hust.tkxdpm.controllers.CardController#checkOut(java.lang.String)}.
	 */
	@Test
	void testCheckCardOutAAAAAAA() {
		new AppUI();
		AppUI.currentStation = currentStation2;
		CardEnum result = cc.checkOut("AAAAAAA");
		assertEquals(CardEnum.CODENOTINDATABASE, result);
	}
	
	/**
	 * Test method for
	 * {@link hust.tkxdpm.controllers.CardController#checkOut(java.lang.String)}.
	 */
	@Test
	void testCheckCardOutAAAA1234() {
		new AppUI();
		AppUI.currentStation = currentStation1;
		CardEnum result = cc.checkOut("AAAA1234");
		assertEquals(CardEnum.CODENOTINDATABASE, result);
	}
	
	/**
	 * Test method for
	 * {@link hust.tkxdpm.controllers.CardController#checkOut(java.lang.String)}.
	 */
	@Test
	void testCheckCardOutNNNNNNNN() {
		new AppUI();
		AppUI.currentStation = currentStation1;
		CardEnum result = cc.checkOut("NNNNNNNN");
		assertEquals(CardEnum.CODENOTINDATABASE, result);
	}



}
