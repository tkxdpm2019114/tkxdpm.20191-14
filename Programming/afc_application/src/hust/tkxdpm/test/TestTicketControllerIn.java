/**
 * 
 */
package hust.tkxdpm.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import hust.tkxdpm.boundaries.*;
import hust.tkxdpm.controllers.*;
import hust.tkxdpm.entities.*;
import hust.tkxdpm.helpers.MyEnum.TicketEnum;

/**
 * @author SpQuyt
 * @date Nov 7, 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
class TestTicketControllerIn {
	private StationController stationControl = new StationController();
	private ArrayList<Station> listStations = stationControl.getAllStations();
	
	private ScanController sc = new ScanController();
	private OWTicketController owtc = new OWTicketController();
	
	private Station currentStation1 = new Station(19, "Saint-Lazare", "Paris"),
			currentStation2 = new Station(20, "Madeleine", "Paris"),
			currentStation3 = new Station(21, "Pyramides", "Paris"),
			currentStation4 = new Station(22, "Chatelet", "Paris"),
			currentStation5 = new Station(23, "Gare de Lyon", "Paris"),
			currentStation6 = new Station(24, "Bercy", "Paris"),
			currentStation7 = new Station(25, "Cour Saint-Emilion", "Paris"),
			currentStation8 = new Station(26, "Bibliothque Francois Mitterrand", "Paris"),
			currentStation9 = new Station(27, "Olympiades", "Paris");

	/**
	 * Test method for
	 * {@link hust.tkxdpm.controllers.TicketController#checkTicketIn(java.lang.String, java.util.ArrayList, hust.tkxdpm.entities.Station)}.
	 */
	@Test
	void testCheckTicketInaaaaaaaa() {
		new AppUI();
		AppUI.currentStation = currentStation6;
		Ticket ticket = sc.convertBarcodeToTicket("aaaaaaaa");
		TicketEnum result = null;
		try {
			result = owtc.checkIn(ticket);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		assertEquals(TicketEnum.SUCCESSIN, result);
	}
	
//	/**
//	 * Test method for
//	 * {@link hust.tkxdpm.controllers.TicketController#checkTicketIn(java.lang.String, java.util.ArrayList, hust.tkxdpm.entities.Station)}.
//	 */
//	@Test
//	void testCheckTicketInbbbbbbbb() {
//		new AppUI();
//		AppUI.currentStation = currentStation5;
//		TicketEnum result =owtc.checkIn("bbbbbbbb");
//
//		assertEquals(TicketEnum.ALREADYCHECKEDIN, result);
//	}
//	
//	/**
//	 * Test method for
//	 * {@link hust.tkxdpm.controllers.TicketController#checkTicketIn(java.lang.String, java.util.ArrayList, hust.tkxdpm.entities.Station)}.
//	 */
//	@Test
//	void testCheckTicketInaaaaaaaa2() {
//		new AppUI();
//		AppUI.currentStation = currentStation2;
//		TicketEnum result =owtc.checkIn("aaaaaaaa");
//
//		assertEquals(TicketEnum.NOTALLOWEDINSTATION, result);
//	}
//	
//	/**
//	 * Test method for
//	 * {@link hust.tkxdpm.controllers.TicketController#checkTicketIn(java.lang.String, java.util.ArrayList, hust.tkxdpm.entities.Station)}.
//	 */
//	@Test
//	void testCheckTicketIncccccccc() {
//		new AppUI();
//		AppUI.currentStation = currentStation2;
//		TicketEnum result =owtc.checkIn("cccccccc");
//
//		assertEquals(TicketEnum.SUCCESSIN, result);
//	}
//	
//	/**
//	 * Test method for
//	 * {@link hust.tkxdpm.controllers.TicketController#checkTicketIn(java.lang.String, java.util.ArrayList, hust.tkxdpm.entities.Station)}.
//	 */
//	@Test
//	void testCheckTicketIndddddddd() {
//		new AppUI();
//		AppUI.currentStation = currentStation3;
//		TicketEnum result =owtc.checkIn("dddddddd");
//
//		assertEquals(TicketEnum.ALREADYCHECKEDIN, result);
//	}
//	
//	/**
//	 * Test method for
//	 * {@link hust.tkxdpm.controllers.TicketController#checkTicketIn(java.lang.String, java.util.ArrayList, hust.tkxdpm.entities.Station)}.
//	 */
//	@Test
//	void testCheckTicketIneeeeeeee() {
//		new AppUI();
//		AppUI.currentStation = currentStation1;
//		TicketEnum result =owtc.checkIn("eeeeeeee");
//
//		assertEquals(TicketEnum.OUTDATEDTICKET, result);
//	}
//	
//	/**
//	 * Test method for
//	 * {@link hust.tkxdpm.controllers.TicketController#checkTicketIn(java.lang.String, java.util.ArrayList, hust.tkxdpm.entities.Station)}.
//	 */
//	@Test
//	void testCheckTicketInabcd() {
//		new AppUI();
//		AppUI.currentStation = currentStation1;
//		TicketEnum result =owtc.checkIn("abcd");
//
//		assertEquals(TicketEnum.CODENOTINDATABASE, result);
//	}
//	
//	/**
//	 * Test method for
//	 * {@link hust.tkxdpm.controllers.TicketController#checkTicketIn(java.lang.String, java.util.ArrayList, hust.tkxdpm.entities.Station)}.
//	 */
//	@Test
//	void testCheckTicketInabcddd12() {
//		new AppUI();
//		AppUI.currentStation = currentStation1;
//		TicketEnum result =owtc.checkIn("abcddd12");
//
//		assertEquals(TicketEnum.CODENOTINDATABASE, result);
//	}
//	
//	/**
//	 * Test method for
//	 * {@link hust.tkxdpm.controllers.TicketController#checkTicketIn(java.lang.String, java.util.ArrayList, hust.tkxdpm.entities.Station)}.
//	 */
//	@Test
//	void testCheckTicketInaabbccdd() {
//		new AppUI();
//		AppUI.currentStation = currentStation1;
//		TicketEnum result =owtc.checkIn("aabbccdd");
//
//		assertEquals(TicketEnum.CODENOTINDATABASE, result);
//	}

}
