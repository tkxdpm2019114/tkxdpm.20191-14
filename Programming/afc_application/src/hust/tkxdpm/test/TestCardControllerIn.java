/**
 * 
 */
package hust.tkxdpm.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import hust.tkxdpm.boundaries.AppUI;
import hust.tkxdpm.controllers.CardController;
import hust.tkxdpm.controllers.StationController;
import hust.tkxdpm.entities.Station;
import hust.tkxdpm.helpers.MyEnum.CardEnum;

/**
 * @author SpQuyt
 * @date Nov 7, 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
class TestCardControllerIn {
	private StationController stationControl = new StationController();
	private ArrayList<Station> listStations = stationControl.getAllStations();
	private CardController cc = CardController.getInstance();
	private Station currentStation1 = new Station(19, "Saint-Lazare", "Paris"),
			currentStation2 = new Station(20, "Madeleine", "Paris"),
			currentStation3 = new Station(21, "Pyramides", "Paris"),
			currentStation4 = new Station(22, "Chatelet", "Paris"),
			currentStation5 = new Station(23, "Gare de Lyon", "Paris"),
			currentStation6 = new Station(24, "Bercy", "Paris"),
			currentStation7 = new Station(25, "Cour Saint-Emilion", "Paris"),
			currentStation8 = new Station(26, "Bibliothque Francois Mitterrand", "Paris"),
			currentStation9 = new Station(27, "Olympiades", "Paris");

	/**
	 * Test method for
	 * {@link hust.tkxdpm.controllers.CardController#checkIn(java.lang.String)}.
	 */
	@Test
	void testCheckCardInAAAAAAAA() {
		new AppUI();
		AppUI.currentStation = currentStation1;
		CardEnum result = cc.checkIn("AAAAAAAA");
		assertEquals(CardEnum.SUCCESSIN, result);
	}
	
	/**
	 * Test method for
	 * {@link hust.tkxdpm.controllers.CardController#checkIn(java.lang.String)}.
	 */
	@Test
	void testCheckCardInCCCCCCCC() {
		new AppUI();
		AppUI.currentStation = currentStation1;
		CardEnum result = cc.checkIn("CCCCCCCC");
		assertEquals(CardEnum.ALREADYCHECKEDIN, result);
	}
	
	/**
	 * Test method for
	 * {@link hust.tkxdpm.controllers.CardController#checkIn(java.lang.String)}.
	 */
	@Test
	void testCheckCardInAAAAAAAa() {
		new AppUI();
		AppUI.currentStation = currentStation2;
		CardEnum result = cc.checkIn("AAAAAAAa");
		assertEquals(CardEnum.CODENOTINDATABASE, result);
	}
	
	/**
	 * Test method for
	 * {@link hust.tkxdpm.controllers.CardController#checkIn(java.lang.String)}.
	 */
	@Test
	void testCheckCardInAAAAAAA() {
		new AppUI();
		AppUI.currentStation = currentStation1;
		CardEnum result = cc.checkIn("AAAAAAA");
		assertEquals(CardEnum.CODENOTINDATABASE, result);
	}
	
	/**
	 * Test method for
	 * {@link hust.tkxdpm.controllers.CardController#checkIn(java.lang.String)}.
	 */
	@Test
	void testCheckCardInHHHHHHHH() {
		new AppUI();
		AppUI.currentStation = currentStation2;
		CardEnum result = cc.checkIn("HHHHHHHH");
		assertEquals(CardEnum.CODENOTINDATABASE, result);
	}
}
