/**
 * 
 */
package hust.tkxdpm.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import hust.tkxdpm.boundaries.AppUI;
import hust.tkxdpm.controllers.StationController;
import hust.tkxdpm.dao.StationDAO;
import hust.tkxdpm.entities.Station;

/**
 * @author SpQuyt
 * @date Nov 7, 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
class TestStation {
	private StationController stationControl = new StationController();
	private ArrayList<Station> listStations = stationControl.getAllStations();
	private Station currentStation1 = new Station(19, "Saint-Lazare", "Paris"),
			currentStation2 = new Station(20, "Madeleine", "Paris"),
			currentStation3 = new Station(21, "Pyramides", "Paris"),
			currentStation4 = new Station(22, "Chatelet", "Paris"),
			currentStation5 = new Station(23, "Gare de Lyon", "Paris"),
			currentStation6 = new Station(24, "Bercy", "Paris"),
			currentStation7 = new Station(25, "Cour Saint-Emilion", "Paris"),
			currentStation8 = new Station(26, "Bibliothque Francois Mitterrand", "Paris"),
			currentStation9 = new Station(27, "Olympiades", "Paris");

	/**
	 * Test method for
	 * {@link hust.tkxdpm.controllers.StationController#getAllStations()}.
	 */
	@Test
	void testGetAllStations() {
		ArrayList<Station> listStation = new ArrayList<Station>();

		try {
			listStation = StationDAO.queryAllStations();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		assertNotNull(listStation);
	}
}
