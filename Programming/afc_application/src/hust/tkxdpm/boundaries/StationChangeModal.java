package hust.tkxdpm.boundaries;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import hust.tkxdpm.entities.Station;
import hust.tkxdpm.helpers.PointLayout;

/**
 * @author SpQuyt
 * @date Oct 25, 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public class StationChangeModal extends JDialog {
	private JPanel contentPanel = new JPanel();
	private int frameWidth = 500;
	private int frameHeight = 230;
	private int startXPoint;
	private int startYPoint;
	private ArrayList<Station> listStations = new ArrayList<Station>();

	public void createFrame(final JDialog frame) {
		frame.setTitle("Change the station");
		frame.setContentPane(this.contentPanel);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		this.startXPoint = PointLayout.startXPoints(this.frameWidth, this.frameHeight);
		this.startYPoint = PointLayout.startYPoints(this.frameWidth, this.frameHeight);

		frame.setBounds(this.startXPoint, this.startYPoint, this.frameWidth, this.frameHeight);
		frame.setVisible(true);
		frame.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				frame.dispose();
			}
		});
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	}

	public void createPanel(final JDialog frame) {
		contentPanel.setBounds(this.startXPoint, this.startYPoint, 469, 184);
		ButtonGroup btnGroup = new ButtonGroup();

		ArrayList<String> listName = new ArrayList<String>();
		for (Station station : listStations) {
			listName.add(station.getName());
		}
		JComboBox<?> stationList = new JComboBox<Object>(listName.toArray());
		stationList.setBounds(152, 41, 288, 30);
		contentPanel.setLayout(null);

		JLabel lblStationsName = new JLabel("Station's name");
		lblStationsName.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblStationsName.setBounds(24, 42, 118, 25);
		contentPanel.add(lblStationsName);
		stationList.setSelectedIndex(listStations.indexOf(AppUI.currentStation));
		contentPanel.add(stationList);

		JRadioButton rdbtnInStation = new JRadioButton("In station");
		rdbtnInStation.setFont(new Font("Tahoma", Font.PLAIN, 14));
		rdbtnInStation.setBounds(82, 90, 109, 23);
		contentPanel.add(rdbtnInStation);

		JRadioButton rdbtnOutStation = new JRadioButton("Out station");
		rdbtnOutStation.setFont(new Font("Tahoma", Font.PLAIN, 14));
		rdbtnOutStation.setBounds(280, 90, 109, 23);
		contentPanel.add(rdbtnOutStation);
		
		if (AppUI.isInStation) {
			rdbtnInStation.setSelected(true);
		} else {
			rdbtnOutStation.setSelected(true);
		}

		JButton btnConfirm = new JButton("Confirm");
		btnConfirm.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnConfirm.setBounds(185, 134, 81, 25);
		contentPanel.add(btnConfirm);
		btnConfirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// All of these for changing Title's name
				String currentName = listStations.get(stationList.getSelectedIndex()).getName();
				String currentInOut = rdbtnInStation.isSelected() ? "In Station" : "Out Station";
				AppUI.lblStationName.setText(currentName + " - " + currentInOut);
				
				// Save to static fields of AppUI
				AppUI.currentStation = listStations.get(stationList.getSelectedIndex());
				AppUI.isInStation = rdbtnInStation.isSelected() ? true : false;
				
				// Close the pop-up
				frame.dispose();
			}
		});

		btnGroup.add(rdbtnInStation);
		btnGroup.add(rdbtnOutStation);
	}

	public StationChangeModal(JDialog frame) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		this.listStations = AppUI.listStations;
		createPanel(frame);
		createFrame(frame);
	}
}
