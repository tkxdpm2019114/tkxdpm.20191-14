package hust.tkxdpm.boundaries;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import hust.tkxdpm.helpers.MyEnum.CardEnum;

/**
 * @author SpQuyt
 * @date Nov 7, 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public class CardDialog {
	private static JLabel lblDisplay = AppUI.cardUI.getLblYourInput();
	public static void displayDialog(CardEnum result) {
		// Reset UI
		lblDisplay = AppUI.cardUI.getLblYourInput();
		switch (result) {
		case SUCCESSIN:
			AppUI.gateUI.requestOpen();
			AppUI.cardUI.fromMenuToInfo();
			lblDisplay.setForeground(Color.green);
			lblDisplay.setText("Check-in successfully!");
			break;
		case SUCCESSOUT:
			AppUI.gateUI.requestOpen();
			AppUI.cardUI.fromMenuToInfo();
			lblDisplay.setForeground(Color.green);
			lblDisplay.setText("Check-out successfully!");
			break;
		case ALREADYCHECKEDIN:
			AppUI.cardUI.fromMenuToInfo();
			lblDisplay.setForeground(Color.red);
			lblDisplay.setText("Card has already been used for check-in!!");
			break;
		case BALANCENOTENOUGH:
			AppUI.cardUI.fromMenuToInfo();
			lblDisplay.setForeground(Color.red);
			lblDisplay.setText("Balance is not enough!!");
			break;
		case NOTCHECKEDINYET:
			AppUI.cardUI.fromMenuToInfo();
			lblDisplay.setForeground(Color.red);
			lblDisplay.setText("Card has not been used for check-in yet!!");
			break;
		default:
			JOptionPane.showMessageDialog(null, "Card code is not in database!");
			break;
		}
	}
}
