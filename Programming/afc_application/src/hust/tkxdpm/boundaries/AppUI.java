package hust.tkxdpm.boundaries;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import hust.tkxdpm.controllers.StationController;
import hust.tkxdpm.entities.Station;
import hust.tkxdpm.helpers.PointLayout;
import hust.tkxdpm.service.DBConnection;

/**
 * @author SpQuyt
 * @date Oct 23, 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public class AppUI extends JFrame {
	private JPanel contentPanel;
	private JFrame frame;
	private int frameWidth = 1200;
	private int frameHeight = 600;
	private int startXPoint;
	private int startYPoint;
	private StationController stationControl = new StationController();

	
	public static ControlGateUI gateUI = new ControlGateUI();
	public static CardScannerUI cardUI = new CardScannerUI();
	public static TicketReaderUI ticketUI = new TicketReaderUI();
	public static ArrayList<Station> listStations = new ArrayList<Station>();
	public static Station currentStation;
	public static boolean isInStation = true;
	public static JLabel lblStationName;
	public static Statement statement = new DBConnection().getStatement();

	public void createFrame() {
		this.frame = new JFrame();
		this.frame.setTitle("Automated Fare Controller");
		this.frame.setContentPane(this.contentPanel);
		this.frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.frame.getContentPane().setLayout(null);

		this.startXPoint = PointLayout.startXPoints(this.frameWidth, this.frameHeight);
		this.startYPoint = PointLayout.startYPoints(this.frameWidth, this.frameHeight);

		this.frame.setBounds(this.startXPoint, this.startYPoint, this.frameWidth, this.frameHeight);
		this.frame.setVisible(true);
		this.frame.setResizable(false);
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void createPanel() {
		this.contentPanel = new JPanel();

		lblStationName = new JLabel(currentStation.getName() + " - " + (isInStation ? "In Station" : "Out Station"));
		lblStationName.setHorizontalAlignment(SwingConstants.CENTER);
		lblStationName.setFont(new Font("Tahoma", Font.BOLD, 28));
		lblStationName.setBounds(231, 33, 745, 43);
		this.contentPanel.add(lblStationName);

		JButton btnChangeStation = new JButton("Change Station");
		btnChangeStation.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnChangeStation.setBounds(986, 33, 135, 43);
		this.contentPanel.add(btnChangeStation);
		btnChangeStation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new StationChangeModal(new JDialog(new JFrame(), true));
			}
		});

		this.contentPanel.add(gateUI);
		this.contentPanel.add(cardUI);
		this.contentPanel.add(ticketUI);
	}

	public AppUI() {

		// Set UI which is suitable for different OS
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		// Get all stations
		listStations = this.stationControl.getAllStations();
		currentStation = listStations.get(0);

		// Create Frame and Panels
		createPanel();
		createFrame();
	}
}
