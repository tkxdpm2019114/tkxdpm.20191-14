package hust.tkxdpm.boundaries;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.LineBorder;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import hust.tkxdpm.controllers.*;
import hust.tkxdpm.entities.*;
import hust.tkxdpm.helpers.MyEnum.CardEnum;
import hust.tkxdpm.helpers.MyEnum.TicketEnum;
import hust.tkxdpm.strategy.PriceCalculateByInOutStation;

/**
 * @author SpQuyt
 * @date Oct 23, 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public class CardScannerUI extends JPanel {
	private int frameWidth = 400;
	private int frameHeight = 580;
	private JList<Object> jlPseudoCode, jlCardInfo;
	private JLabel lblTitle, lblDescription, lblYourInput;
	private JTextField tfCardInput;
	private JButton btnConfirm, btnBack;

	private String pseudoCodeList[] = { "AAAAAAAA", "BBBBBBBB", "CCCCCCCC", "DDDDDDDD" };
	private Card card;
	private CardController ppCardController = new PPCardController();

	public JLabel getLblYourInput() {
		return lblYourInput;
	}

	public void setLblYourInput(JLabel lblYourInput) {
		this.lblYourInput = lblYourInput;
	}

	// Create a specific JTextField which has character limit and upper-case only
	class JTextFieldLimit extends PlainDocument {
		private static final long serialVersionUID = 1L;
		private int limit;

		JTextFieldLimit(int limit) {
			super();
			this.limit = limit;
		}

		public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
			if (str == null)
				return;

			if ((getLength() + str.length()) <= limit) {
				super.insertString(offset, str.toUpperCase(), attr);
			}
		}
	}

	public CardScannerUI() {

		// Set UI which is suitable for different OS
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		setBorder(new LineBorder(new Color(0, 0, 0), 1));
		this.setLayout(null);
		this.setBounds(0, 100, this.frameWidth, this.frameHeight - 110);

		this.displayMenu();
	}

	// When swapping from MENU to CARD INFO
	public void fromMenuToInfo() {
		removeMenu();
		displayCardInfo();
		revalidate();
		repaint();
	}

	// When swapping from CARD INFO to MENU
	public void fromInfoToMenu() {
		removeCardInfo();
		displayMenu();
		revalidate();
		repaint();
	}

	public void removeMenu() {
		this.remove(lblTitle);
		this.remove(lblDescription);
		this.remove(jlPseudoCode);
		this.remove(tfCardInput);
		this.remove(btnConfirm);
		lblYourInput.setText("");
	}

	public void removeCardInfo() {
		this.remove(jlCardInfo);
		this.remove(btnBack);
		lblYourInput.setText("");
	}

	public void displayMenu() {
		lblTitle = new JLabel("Choosing a card");
		lblTitle.setFont(new Font("Tahoma", Font.BOLD, 24));
		lblTitle.setBounds(103, 11, 205, 59);
		this.add(lblTitle);

		lblDescription = new JLabel("These are existing cards...");
		lblDescription.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblDescription.setBounds(25, 63, 194, 37);
		this.add(lblDescription);

		jlPseudoCode = new JList<Object>(pseudoCodeList);
		jlPseudoCode.setFont(new Font("Tahoma", Font.PLAIN, 17));
		jlPseudoCode.setLocation(25, 110);
		jlPseudoCode.setSize(350, 200);
		this.add(jlPseudoCode);

		lblYourInput = new JLabel("Your input");
		lblYourInput.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblYourInput.setBounds(34, 313, 341, 34);
		this.add(lblYourInput);

		tfCardInput = new JTextField();
		tfCardInput.setFont(new Font("Tahoma", Font.PLAIN, 16));
		tfCardInput.setColumns(10);
		tfCardInput.setBounds(34, 358, 341, 37);
		tfCardInput.setDocument(new JTextFieldLimit(8));
		this.add(tfCardInput);

		btnConfirm = new JButton("OK");
		btnConfirm.setBounds(286, 419, 89, 23);
		btnConfirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ScanController scanController = new ScanController();
				// Check if the text has full 8 characters?
				if (tfCardInput.getText().length() == 8) {

					// Check if the text is all letters?
					if (tfCardInput.getText().matches("^[a-zA-Z]+$")) {
						card = scanController.convertBarcodeToCard(tfCardInput.getText());
						goToCardController(card);
					} else {
						JOptionPane.showMessageDialog(null, "Barcode contains letters only!");
					}
				} else {
					JOptionPane.showMessageDialog(null, "Barcode must have exactly 8 characters!");
				}

			}
		});
		this.add(btnConfirm);
	}
	
	private void goToCardController(Card card) {
		try {
			CardEnum result;
			// If customer is getting in the station
			if (AppUI.isInStation) {
				if (card instanceof PrepaidCard) {
					result = ppCardController.checkIn(card);
					CardDialog.displayDialog(result);
				}
			} else {
				if (card instanceof PrepaidCard) {
					result = ppCardController.checkOut(card, new PriceCalculateByInOutStation());
					CardDialog.displayDialog(result);
				}
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
		}

	}

	public void displayCardInfo() {
		ArrayList<String> cardInfo = new ArrayList<String>();
		String cardId = card.getCardId();
		String balance = Double.toString(card.getBalance());
		String customerName = card.getCustomer().getName().toUpperCase();
		String customerPhone = card.getCustomer().getPhone();

		cardInfo.add("PRE-PAID CARD");
		cardInfo.add("Id: " + cardId);
		cardInfo.add("Balance: " + balance);
		cardInfo.add("Customer's name: " + customerName);
		cardInfo.add("Customer's phone: " + customerPhone);
		cardInfo.add(card.getCheckInStation() == null ? "Check-in station: NULL"
				: "Check-in station: " + card.getCheckInStation().getName().toUpperCase());
		cardInfo.add(AppUI.isInStation ? "" : String.format("Total amounts: %f", ((PPCardController) ppCardController).getTotalAmount()));

		jlCardInfo = new JList<Object>(cardInfo.toArray());
		jlCardInfo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		jlCardInfo.setLocation(25, 110);
		jlCardInfo.setSize(350, 200);
		this.add(jlCardInfo);

		btnBack = new JButton("Back");
		btnBack.setBounds(286, 419, 89, 23);
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblYourInput.setForeground(Color.black);
				fromInfoToMenu();
			}
		});
		this.add(btnBack);
	}
}
