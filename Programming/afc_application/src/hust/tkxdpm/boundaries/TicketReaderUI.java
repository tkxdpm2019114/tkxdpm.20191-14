package hust.tkxdpm.boundaries;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.LineBorder;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import hust.tkxdpm.controllers.*;
import hust.tkxdpm.entities.*;
import hust.tkxdpm.helpers.MyEnum.TicketEnum;
import hust.tkxdpm.helpers.MyFormatter;
import hust.tkxdpm.strategy.PriceCalculateByInOutStation;

/**
 * @author SpQuyt
 * @date Oct 23, 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public class TicketReaderUI extends JPanel {
	private int frameWidth = 400;
	private int frameHeight = 580;
	private JList<Object> jlPseudoCode, jlticketInfo;
	private JLabel lblTitle, lblDescription, lblYourInput;
	private JTextField tfTicketInput;
	private JButton btnConfirm, btnBack;

	private String pseudoCodeList[] = { "aaaaaaaa", "bbbbbbbb", "cccccccc", "dddddddd", "eeeeeeee" };
	private Ticket ticket;
	private TicketController owTicketController = new OWTicketController();
	private TicketController tfTicketController = new TFTicketController();

	public JLabel getLblYourInput() {
		return lblYourInput;
	}

	public void setLblYourInput(JLabel lblYourInput) {
		this.lblYourInput = lblYourInput;
	}

	// Create a specific JTextField which has character limit and lower-case only
	class JTextFieldLimit extends PlainDocument {
		private int limit;

		JTextFieldLimit(int limit) {
			super();
			this.limit = limit;
		}

		JTextFieldLimit(int limit, boolean upper) {
			super();
			this.limit = limit;
		}

		public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
			if (str == null)
				return;

			if ((getLength() + str.length()) <= limit) {
				super.insertString(offset, str.toLowerCase(), attr);
			}
		}
	}

	public TicketReaderUI() {
		// Set UI which is suitable for different OS
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		setBorder(new LineBorder(new Color(0, 0, 0), 1));
		this.setLayout(null);
		this.setBounds(794, 100, this.frameWidth, this.frameHeight - 110);

		this.displayMenu();
	}

	// When swapping from MENU to CARD INFO
	public void fromMenuToInfo() {
		removeMenu();
		displayTicketInfo();
		revalidate();
		repaint();
	}

	// When swapping from CARD INFO to MENU
	public void fromInfoToMenu() {
		removeTicketInfo();
		displayMenu();
		revalidate();
		repaint();
	}

	public void removeMenu() {
		this.remove(lblTitle);
		this.remove(lblDescription);
		this.remove(jlPseudoCode);
		this.remove(tfTicketInput);
		this.remove(btnConfirm);
		lblYourInput.setText("");
	}

	public void removeTicketInfo() {
		this.remove(jlticketInfo);
		this.remove(btnBack);
		lblYourInput.setText("");
	}

	public void displayMenu() {
		lblTitle = new JLabel("Choosing a ticket");
		lblTitle.setFont(new Font("Tahoma", Font.BOLD, 24));
		lblTitle.setBounds(92, 11, 230, 59);
		add(lblTitle);

		lblDescription = new JLabel("These are existing tickets...");
		lblDescription.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblDescription.setBounds(25, 63, 194, 37);
		add(lblDescription);

		jlPseudoCode = new JList<Object>(pseudoCodeList);
		jlPseudoCode.setFont(new Font("Tahoma", Font.PLAIN, 17));
		jlPseudoCode.setLocation(25, 110);
		jlPseudoCode.setSize(350, 200);
		add(jlPseudoCode);

		lblYourInput = new JLabel("Your input");
		lblYourInput.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblYourInput.setBounds(25, 312, 350, 34);
		add(lblYourInput);

		tfTicketInput = new JTextField();
		tfTicketInput.setFont(new Font("Tahoma", Font.PLAIN, 16));
		tfTicketInput.setColumns(10);
		tfTicketInput.setBounds(33, 357, 341, 37);
		tfTicketInput.setDocument(new JTextFieldLimit(8));
		add(tfTicketInput);

		btnConfirm = new JButton("OK");
		btnConfirm.setBounds(287, 419, 89, 23);
		btnConfirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ScanController scanController = new ScanController();
				// Check if the text has full 8 characters?
				if (tfTicketInput.getText().length() == 8) {
					// Check if the text is all letters?
					if (tfTicketInput.getText().matches("^[a-zA-Z]+$")) {
						ticket = scanController.convertBarcodeToTicket(tfTicketInput.getText());
						goToTicketController(ticket);
					} else {
						JOptionPane.showMessageDialog(null, "Barcode contains letters only!");
					}
				} else {
					JOptionPane.showMessageDialog(null, "Barcode must have exactly 8 characters!");
				}

			}
		});
		add(btnConfirm);
	}

	private void goToTicketController(Ticket ticket) {
		try {
			TicketEnum result;
			// If customer is getting in the station
			if (AppUI.isInStation) {
				if (ticket instanceof OneWayTicket) {
					result = owTicketController.checkIn(ticket);
					TicketDialog.displayDialog(result);
				} else {
					result = tfTicketController.checkIn(ticket);
					TicketDialog.displayDialog(result);
				}
			} else {
				if (ticket instanceof OneWayTicket) {
					result = owTicketController.checkOut(ticket, new PriceCalculateByInOutStation());
					TicketDialog.displayDialog(result);
				} else {
					result = tfTicketController.checkOut(ticket, new PriceCalculateByInOutStation());
					TicketDialog.displayDialog(result);
				}
			}
		} catch (ClassNotFoundException | SQLException | NullPointerException e) {
			e.printStackTrace();
		}

	}

	public void displayTicketInfo() {
		ArrayList<String> ticketInfo = new ArrayList<String>();
		String ticketId = ticket.getTicketId();
		String customerName = ticket.getCustomer().getName().toUpperCase();
		String customerPhone = ticket.getCustomer().getPhone();

		// Check if ticket is One-Way type?
		if (ticket instanceof OneWayTicket) {
			String defaultInStationName = ((OneWayTicket) ticket).getDefaultInStation().getName().toUpperCase();
			String defaultOutStationName = ((OneWayTicket) ticket).getDefaultOutStation().getName().toUpperCase();

			ticketInfo.add("ONE WAY TICKET");
			ticketInfo.add("Id: " + ticketId);
			ticketInfo.add("Customer's name: " + customerName);
			ticketInfo.add("Customer's phone: " + customerPhone);
			ticketInfo.add("Default In Station: " + defaultInStationName);
			ticketInfo.add("Default Out Station: " + defaultOutStationName);
			ticketInfo.add(ticket.getCheckInStation() == null ? "Check-in station: NULL"
					: "Check-in station: " + ticket.getCheckInStation().getName().toUpperCase());
			ticketInfo.add(((OneWayTicket) ticket).getCheckOutStation() == null ? "Check-out station: NULL"
					: "Check-out station: " + ((OneWayTicket) ticket).getCheckOutStation().getName().toUpperCase());
		} else {
			String expiredTime = MyFormatter.formatDateTime(((TwentyFourTicket) ticket).getExpiredTime());
			String currentTime = MyFormatter.formatDateTime(System.currentTimeMillis());

			ticketInfo.add("24-HOUR TICKET");
			ticketInfo.add("Id: " + ticketId);
			ticketInfo.add("Customer's name: " + customerName);
			ticketInfo.add("Customer's phone: " + customerPhone);
			ticketInfo.add("Expired Time: " + expiredTime);
			ticketInfo.add("Current Time: " + currentTime);
			ticketInfo.add(ticket.getCheckInStation() == null ? "Check-in station: NULL"
					: "Check-in station: " + ticket.getCheckInStation().getName().toUpperCase());
		}

		jlticketInfo = new JList<Object>(ticketInfo.toArray());
		jlticketInfo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		jlticketInfo.setLocation(25, 110);
		jlticketInfo.setSize(350, 200);
		this.add(jlticketInfo);

		btnBack = new JButton("Back");
		btnBack.setBounds(286, 419, 89, 23);
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblYourInput.setForeground(Color.black);
				fromInfoToMenu();
			}
		});
		this.add(btnBack);
	}
}
