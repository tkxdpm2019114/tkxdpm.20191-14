package hust.tkxdpm.boundaries;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import hust.tkxdpm.helpers.MyEnum.TicketEnum;

/**
 * @author SpQuyt
 * @date Nov 7, 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public class TicketDialog {
	private static JLabel lblDisplay = AppUI.ticketUI.getLblYourInput();
	public static void displayDialog(TicketEnum result) {
		// Reset UI
		lblDisplay = AppUI.ticketUI.getLblYourInput();
		switch (result) {
		case SUCCESSIN:
			AppUI.gateUI.requestOpen();
			AppUI.ticketUI.fromMenuToInfo();
			lblDisplay.setForeground(Color.green);
			lblDisplay.setText("Check-in successfully!");
			break;
		case SUCCESSOUT:
			AppUI.gateUI.requestOpen();
			AppUI.ticketUI.fromMenuToInfo();
			lblDisplay.setForeground(Color.green);
			lblDisplay.setText("Check-out successfully!");
			break;
		case NOTALLOWEDINSTATION:
			AppUI.ticketUI.fromMenuToInfo();
			lblDisplay.setForeground(Color.red);
			lblDisplay.setText("Not a valid check-in station for this ticket!!");
			break;
		case NOTALLOWEDOUTSTATION:
			AppUI.ticketUI.fromMenuToInfo();
			lblDisplay.setForeground(Color.red);
			lblDisplay.setText("Check-out station must not be check-in station!!");
			break;
		case ALREADYCHECKEDIN:
			AppUI.ticketUI.fromMenuToInfo();
			lblDisplay.setForeground(Color.red);
			lblDisplay.setText("Ticket has already been used for check-in!!");
			break;
		case NOTCHECKEDINYET:
			AppUI.ticketUI.fromMenuToInfo();
			lblDisplay.setForeground(Color.red);
			lblDisplay.setText("Ticket has not been used for check-in yet!!");
			break;
		case OUTDATEDTICKET:
			AppUI.ticketUI.fromMenuToInfo();
			lblDisplay.setForeground(Color.red);
			lblDisplay.setText("This ticket is expired!!");
			break;
		case BALANCENOTENOUGH:
			AppUI.ticketUI.fromMenuToInfo();
			lblDisplay.setForeground(Color.red);
			lblDisplay.setText("Balance is not enough!!");
			break;
		default:
			JOptionPane.showMessageDialog(null, "Ticket code is not in database!");
			break;
		}
	}
}
