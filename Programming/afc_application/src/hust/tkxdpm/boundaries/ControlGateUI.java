package hust.tkxdpm.boundaries;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;


/**
 * @author SpQuyt
 * @date Oct 25, 2019
 * @project afc_application
 * @teacherName Nguyen Thi Thu Trang
 * @class 111589
 *
 * @description Program to simulate the Automated Fare Controller
 */
public class ControlGateUI extends JPanel {
	private int frameWidth = 390;
	private int frameHeight = 580;
	private BufferedImage imgOpen = null, imgClose = null;
	private JLabel picLabel = null;
	private Timer mTimer = new Timer();
	private TimerTask mTask = new TimerTask() {
		@Override
		public void run() {
			picLabel.setIcon(new ImageIcon(imgOpen));
		}
	};

	public void requestOpen() {
		mTimer.scheduleAtFixedRate(mTask, 0, 1000);
		mTimer.schedule(new TimerTask() {
			public void run() {
				mTimer.cancel();
				mTask.cancel();
				mTimer = new Timer();
				mTask = new TimerTask() {
					@Override
					public void run() {
						picLabel.setIcon(new ImageIcon(imgOpen));
					}
				};
				picLabel.setIcon(new ImageIcon(imgClose));
			}
		}, 3000);
	}

	public ControlGateUI() {
		try {
			imgClose = ImageIO.read(new File("closegate.jpg"));
			imgOpen = ImageIO.read(new File("opengate.jpg"));
		} catch (IOException e2) {
			e2.printStackTrace();
		}

		this.setLayout(null);
		this.setBounds(404, 100, this.frameWidth, this.frameHeight);

		picLabel = new JLabel(new ImageIcon(imgClose));
		picLabel.setSize(385, 400);
		add(picLabel);
	}

}
