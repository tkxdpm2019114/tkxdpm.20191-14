#Phân công công việc

**1. Lên ý tưởng tổng quan** 

* Quốc

**2. User Interface Design**

* `CardScannerUI` → **RATHANA**
* `TicketReaderUI` → **YOS SAROEUN**

**3. System Interface Design** 

* `ControlGateUI` → **RATHANA**

**3. Overview class diagram** 

* **QUỐC**

**4. Class details** 

* **SƠN**

**5. ER Diagram** 

* **QUỐC**

**4. Database design** 

* `Diagram` → **SƠN**
* `Database details` → **QUỐC**


#Tiến độ công việc

* Rathana: Hoàn thành đúng hẹn
* Sơn: Hoàn thành đúng hẹn
* Yos Saroeun: Hoàn thành đúng hẹn
* Quốc: Hoàn thành đúng hẹn
