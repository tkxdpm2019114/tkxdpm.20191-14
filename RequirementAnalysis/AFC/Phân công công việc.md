#Phân công công việc

**1.Usecase tổng quan** 

* Quốc + Sơn

**2. Đặc tả UC**

* `UC001: Kiểm tra vé hợp lệ` + `UC002: Kiểm tra thẻ hợp lệ` → **RATHANA**
* `UC003: Từ chối vé` + `UC004: Từ chối thẻ` → **YOS SAROEUN**
* `UC005: Chấp nhận vé` + `UC006: Chấp nhận thẻ` → **QUỐC**
* `UC007: Cập nhật trang thái vé 1 chiều` + `UC008: Cập nhật số dư thẻ` → **SƠN**

**3. Đặc tả phi chức năng** 

* `Tính khả dụng + Tính tin cậy` → **RATHANA**
* `Tính bảo mật + Tính duy trì` → **YOS SAROEUN**
* `Công nghệ lưu trữ + Môi trường` → **QUỐC**
* `Hiệu suất + Tương thích` → **SƠN**

**3. Glossary** 

* `Depot/Station computers` → **RATHANA**
* `Devices to read/write media` → **YOS SAROEUN**
* `Back office + Clearing house` → **QUỐC**
* `Fare media` → **SƠN**


#Tiến độ công việc

* Rathana: Hoàn thành đúng tiến độ
* Sơn: Hoàn thành đúng tiến độ
* Yos Saroeun: Hoàn thành đúng tiến độ
* Quốc: Hoàn thành đúng tiến độ
