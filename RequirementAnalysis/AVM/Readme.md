#Phân công công việc
**1.** Usecase tổng quan: Quốc + Sơn

**2.** Luồng sự kiện
* Mua thẻ: Rathana
* Mua vé: Sơn
* Thanh toán: Quốc
* Nạp thẻ: Yos

**3.** Tiến độ công việc
* Rathana + Sơn + Quốc: Hoàn thành đúng deadline
* Yos do có việc bận nên không kịp vẽ luồng sự kiện. Chuyển việc qua cho Sơn. 
